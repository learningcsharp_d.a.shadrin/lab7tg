﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace lab7tg
{
    public class TicTacToeGame
    {
        #region Поля

        /// <summary>
        /// Игровое поле для крестиков и ноликов.
        /// </summary>
        public List<List<string>> board;
        
        /// <summary>
        /// Массив игроков (крестик и нолик).
        /// </summary>
        public string[] players = { "X", "O" };

        /// <summary>
        /// Текущий игрок, чей ход.
        /// </summary>
        public string turn;

        /// <summary>
        /// Флаг, указывающий, является ли текущий ход компьютерным.
        /// </summary>
        public bool isComputerTurn;

        #endregion

        #region Методы

        /// <summary>
        /// Получить текущего игрока (крестик или нолик).
        /// </summary>
        /// <returns>Текущий игрок.</returns>
        public string GetTurn()
        {
            return turn;
        }

        /// <summary>
        /// Получить состояние игрового поля.
        /// </summary>
        /// <returns>Игровое поле в виде списка списков строк.</returns>
        public List<List<string>> GetBoard()
        {
            return board;
        }

        /// <summary>
        /// Проверить, является ли ход допустимым (ячейка свободна).
        /// </summary>
        /// <param name="move">Номер хода (ячейки).</param>
        /// <returns>True, если ход допустим, иначе False.</returns>
        public bool IsValidMove(int move)
        {
            var row = (move - 1) / 3;
            var col = (move - 1) % 3;
            return board[row][col] == "-";
        }

        /// <summary>
        /// Выполняет ход в игре, помещая символ текущего игрока в указанную ячейку на игровом поле.
        /// </summary>
        /// <param name="move">Номер ячейки для выполнения хода (от 1 до 9).</param>
        /// <returns>True, если ход был успешно выполнен, иначе False (если ячейка уже занята или недопустима).</returns>
        public bool MakeMove(int move)
        {
            var row = (move - 1) / 3;
            var col = (move - 1) % 3;

            if (IsValidMove(move))
            {
                board[row][col] = turn;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Проверяет, был ли завершен ход игры и возвращает результат (победа, проигрыш, ничья или продолжение).
        /// </summary>
        /// <param name="result">Результат игры ("Win", "Lose", "Draw" или "Continue").</param>
        /// <returns>True, если игра завершена, иначе False.</returns>
        public bool IsGameFinished(out string result)
        {
            if (HasWon("X"))
            {
                result = "Win";
                return true;
            }
            else if (HasWon("O"))
            {
                result = "Lose";
                return true;
            }
            else if (IsFull())
            {
                result = "Draw";
                return true;
            }
            else
            {
                result = "Continue";
                return false;
            }
        }

        /// <summary>
        /// Проверяет, выиграл ли игрок с заданным знаком (X или O).
        /// </summary>
        /// <param name="mark">Знак игрока (X или O).</param>
        /// <returns>True, если игрок выиграл, иначе False.</returns>
        public bool HasWon(string mark)
        {
            // Check rows
            if (board.Any(row => row.All(cell => cell == mark)))
                return true;

            // Check columns
            for (int i = 0; i < 3; i++)
            {
                if (board.All(row => row[i] == mark))
                    return true;
            }

            // Check diagonals
            if (board[0][0] == mark && board[1][1] == mark && board[2][2] == mark)
                return true;

            if (board[0][2] == mark && board[1][1] == mark && board[2][0] == mark)
                return true;

            return false;
        }

        /// <summary>
        /// Проверяет, заполнено ли игровое поле (все ячейки заняты).
        /// </summary>
        /// <returns>True, если игровое поле заполнено, иначе False.</returns>
        public bool IsFull()
        {
            return board.All(row => row.All(cell => cell != "-"));
        }

        /// <summary>
        /// Сбрасывает игру в начальное состояние.
        /// </summary>
        public void ResetGame()
        {
            this.board = new List<List<string>>()
        {
            new List<string>() { "-", "-", "-" },
            new List<string>() { "-", "-", "-" },
            new List<string>() { "-", "-", "-" }
        };

            this.turn = players[new Random().Next(0, 2)];
        }
        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор класса TicTacToeGame. Инициализирует игровое поле и текущего игрока.
        /// </summary>
        public TicTacToeGame()
        {
            this.board = new List<List<string>>()
            {
                new List<string>() { "-", "-", "-" },
                new List<string>() { "-", "-", "-" },
                new List<string>() { "-", "-", "-" }
            };

            this.turn = players[new Random().Next(0, 2)];
            isComputerTurn = (turn == "O");
        }
        #endregion
    }
}
