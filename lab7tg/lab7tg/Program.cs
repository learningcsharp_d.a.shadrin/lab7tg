﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace lab7tg
{
    class Program
    {
        #region Константы
        private static readonly string BOT_TOKEN = "6370471148:AAFkyRiurHYmPd62eU18-yaztC7IYNjqFhA";
        #endregion

        static async Task Main(string[] args)
        {
            var bot = new TelegramBot(BOT_TOKEN);
            var botClient = bot.botClient;
            var game = bot.game;

            game.board = new List<List<string>>()
            {
                new List<string>() { "-", "-", "-" },
                new List<string>() { "-", "-", "-" },
                new List<string>() { "-", "-", "-" }
            };

            game.turn = game.players[new Random().Next(0, 2)];

            using var cts = new CancellationTokenSource();

            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = { }
            };

            botClient.StartReceiving(
             async (telegramBot, update, ct) => await HandleUpdatesAsync(telegramBot, update, ct, game, bot),
             async (telegramBot, exception, ct) => await HandleErrorAsync(telegramBot, exception, ct));


            var me = await botClient.GetMeAsync();

            Console.WriteLine($"Listening for updates from @{me.Username}");
            Console.ReadLine();

            cts.Cancel();
        }

        #region Методы
        private static async Task HandleErrorAsync(ITelegramBotClient client, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException
                    => $"Telegram API Error:\n{apiRequestException.ErrorCode}\n{apiRequestException.Message}",
                _ => exception.ToString()
            };
            Console.WriteLine(ErrorMessage);
        }

        private static async Task HandleMessage(ITelegramBotClient botClient, Message message, TicTacToeGame game, TelegramBot bot)
        {
            if (message.Text == "/start")
            {
                await bot.StartGame(message.Chat.Id);
                return;
            }

            await botClient.SendTextMessageAsync(message.Chat.Id, $"You said:\n{message.Text}");
        }

        private static async Task HandleCallbackQuery(ITelegramBotClient botClient, CallbackQuery callbackQuery, TicTacToeGame game, TelegramBot bot)
        {
            var chatId = callbackQuery.Message.Chat.Id;
            var data = callbackQuery.Data;

            if (int.TryParse(data, out int userMove))
            {
                if (userMove >= 1 && userMove <= 9)
                {
                    int row = (userMove - 1) / 3;
                    int col = (userMove - 1) % 3;

                    if (game.board[row][col] == "-")
                    {
                        game.board[row][col] = game.turn;
                        await bot.DisplayBoard(chatId);

                        if (game.HasWon(game.turn))
                        {
                            await botClient.SendTextMessageAsync(chatId, $"Player {game.turn} has won! 🎉");
                            game.ResetGame();
                        }
                        else if (game.IsFull())
                        {
                            await botClient.SendTextMessageAsync(chatId, "The game is a tie! 😐");
                            game.ResetGame();
                        }
                        else
                        {
                            game.turn = (game.turn == "X") ? "O" : "X";
                            await botClient.SendTextMessageAsync(chatId, $"It's {game.turn}'s turn.");
                        }

                        if (!game.HasWon(game.turn) && !game.IsFull())
                        {
                            game.isComputerTurn = true;
                            await bot.MakeComputerMove(chatId);
                        }
                    }
                    else
                    {
                        await botClient.SendTextMessageAsync(chatId, "That position is already taken. Please choose another one.");
                    }
                }
                else
                {
                    await botClient.SendTextMessageAsync(chatId, "Please enter a number between 1 and 9.");
                }
            }
            else
            {
                await botClient.SendTextMessageAsync(chatId, "Please enter a valid number.");
            }
        }

        private static async Task HandleUpdatesAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken, TicTacToeGame game, TelegramBot bot)
        {
            if (update.Type == UpdateType.Message && update?.Message?.Text != null)
            {
                await HandleMessage(botClient, update.Message, game, bot);
                return;
            }

            if (update.Type == UpdateType.CallbackQuery)
            {
                await HandleCallbackQuery(botClient, update.CallbackQuery, game, bot);
                return;
            }
        }
        #endregion

    }
}

