﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace lab7tg
{
    public class TelegramBot
    {
        #region Поля

        /// <summary>
        /// Клиент Telegram Bot API для взаимодействия с ботом.
        /// </summary>
        public ITelegramBotClient botClient;

        /// <summary>
        /// Экземпляр игры Tic-Tac-Toe, используемый в боте.
        /// </summary>
        public TicTacToeGame game;

        #endregion

        #region Методы

        /// <summary>
        /// Начинает новую игру Tic-Tac-Toe в чате с указанным идентификатором.
        /// </summary>
        /// <param name="chatId">Идентификатор чата, в котором начинается игра.</param>
        /// <returns>Асинхронная задача для управления началом игры.</returns>
        public async Task StartGame(long chatId)
        {
            await botClient.SendTextMessageAsync(chatId, "Welcome to Tic-Tac-Toe!");
            await DisplayBoard(chatId);
            await botClient.SendTextMessageAsync(chatId, $"Player X is {game.turn}. It's {game.turn}'s turn. Please choose a move using the buttons below:");

            game.isComputerTurn = (game.turn == "O");
            if (game.isComputerTurn)
            {
                await MakeComputerMove(chatId);
            }
        }

        /// <summary>
        /// Отображает текущее игровое поле в чате с указанным идентификатором.
        /// </summary>
        /// <param name="chatId">Идентификатор чата, в котором отображается игровое поле.</param>
        /// <returns>Асинхронная задача для отправки сообщения с игровым полем.</returns>
        public async Task DisplayBoard(long chatId)
        {
            var symbols = new Dictionary<string, string>
        {
            { "-", "⬜️" },
            { "X", "❌" },
            { "O", "⭕️" }
        };

            var inlineKeyboard = new InlineKeyboardMarkup(new[]
            {
            new[]
            {
                InlineKeyboardButton.WithCallbackData("1", "1"),
                InlineKeyboardButton.WithCallbackData("2", "2"),
                InlineKeyboardButton.WithCallbackData("3", "3"),
            },
            new[]
            {
                InlineKeyboardButton.WithCallbackData("4", "4"),
                InlineKeyboardButton.WithCallbackData("5", "5"),
                InlineKeyboardButton.WithCallbackData("6", "6"),
            },
            new[]
            {
                InlineKeyboardButton.WithCallbackData("7", "7"),
                InlineKeyboardButton.WithCallbackData("8", "8"),
                InlineKeyboardButton.WithCallbackData("9", "9"),
            },
        });

            string boardStr = string.Join("\n", game.board.Select(row => string.Join("", row.Select(cell => symbols[cell]))));

            await botClient.SendTextMessageAsync(chatId, boardStr, replyToMessageId: null);

            await botClient.SendTextMessageAsync(chatId, "Select a cell to make your move:", replyMarkup: inlineKeyboard);
        }

        /// <summary>
        /// Выполняет ход за компьютерного игрока.
        /// </summary>
        /// <param name="chatId">Идентификатор чата, в котором происходит игра.</param>
        /// <returns>Асинхронная задача для управления ходом компьютерного игрока.</returns>
        public async Task MakeComputerMove(long chatId)
        {
            await Task.Delay(1000); // Delay for realism

            var random = new Random();
            int computerMove;
            do
            {
                computerMove = random.Next(1, 10);
            } while (!game.IsValidMove(computerMove));

            int row = (computerMove - 1) / 3;
            int col = (computerMove - 1) % 3;

            game.board[row][col] = game.turn;
            await DisplayBoard(chatId);

            if (game.HasWon(game.turn))
            {
                await botClient.SendTextMessageAsync(chatId, $"Player {game.turn} has won! 🎉");
                game.ResetGame();
            }
            else if (game.IsFull())
            {
                await botClient.SendTextMessageAsync(chatId, "The game is a tie! 😐");
                game.ResetGame();
            }
            else
            {
                game.turn = (game.turn == "X") ? "O" : "X";
                await botClient.SendTextMessageAsync(chatId, $"It's {game.turn}'s turn.");
            }
        }
        #endregion

        #region Конструкторы

        /// <summary>
        /// Инициализирует новый экземпляр класса TelegramBot с указанным токеном бота.
        /// </summary>
        /// <param name="botToken">Токен Telegram бота для авторизации.</param>
        public TelegramBot(string botToken)
        {
            botClient = new TelegramBotClient(botToken);
            game = new TicTacToeGame();
        }
        #endregion
    }

}
